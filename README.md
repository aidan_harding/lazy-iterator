# Lazy Iterators

For working on iterable Apex object (e.g. Lists) with lazy-evaluated functions. 

For example:

    private static List<Account> accounts = new List<Account>{
            new Account(NumberOfEmployees = 1),
            new Account(NumberOfEmployees = 2),
            new Account(NumberOfEmployees = 3),
            new Account(NumberOfEmployees = 4),
            new Account(NumberOfEmployees = 5)
    };
    List<Account> result = (List<Account>)new LazySObjectIterator(accounts.iterator())
            .filter(new NumberOfEmployeesIsOdd())
            .mapValues(new DoubleNumberOfEmployeesMapping())
            .toList(new List<Account>());

Also includes convenience classes for working with a trigger context e.g.

    public void handleAfterUpdate(List<Contact> oldList, List<Contact> newList) {
        insert (List<Task>)new LazyTriggerContextPairIterator(oldList, newList)
                .filter(new BirthdayChangedFromNull())
                .mapValues(new TriggerContextPair.NewRecordFromPair())
                .mapValues(new BirthdayTaskFromContact())
                .toList(new List<Task>());
    }


For full details, see https://nebulaconsulting.co.uk/insights/using-lazy-evaluation-to-write-salesforce-apex-code-without-for-loops

