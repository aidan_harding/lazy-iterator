/**
 * @author aidan@nebulaconsulting.co.uk
 * @date 2019-03-22
 * @description Iterates over the pair of lists that you get from a Trigger context, returning them in Pairs
 *
 * MIT License
 *
 * Copyright (c) 2018 Aidan Harding, Nebula Consulting
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

global class TriggerContextIterator implements Iterator<TriggerContextPair> {

    private Iterator<SObject> newIterator;
    private Iterator<SObject> oldIterator;

    global TriggerContextIterator(List<SObject> oldList, List<SObject> newList) {
        // NB, these can't be ternaries because: "Operation cast is not allowed on type: System.ListIterator<SObject>"
        if(newList == null) {
            newIterator = new NullIterator();
        } else {
            newIterator = newList.iterator();
        }
        if(oldList == null) {
            oldIterator = new NullIterator();
        } else {
            oldIterator = oldList.iterator();
        }
    }

    global Boolean hasNext() {
        return newIterator.hasNext() && oldIterator.hasNext();
    }

    global TriggerContextPair next() {
        return new TriggerContextPair(oldIterator.next(), newIterator.next());
    }

    private class NullIterator implements Iterator<SObject> {
        public Boolean hasNext() {
            return true;
        }

        public SObject next() {
            return null;
        }
    }
}